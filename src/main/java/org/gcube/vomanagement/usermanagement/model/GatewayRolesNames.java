package org.gcube.vomanagement.usermanagement.model;
/**
 * @deprecated As of gCube 5.13 version please refer to <a href="https://code-repo.d4science.org/gCubeSystem/oidc-library">oidc-library</a> 
 *
 */
@Deprecated
public enum GatewayRolesNames {
		ACCOUNTING_MANAGER("Accounting-Manager"),
		CATALOGUE_ADMIN("Catalogue-Admin"),
		CATALOGUE_EDITOR("Catalogue-Editor"),
		CATALOGUE_MANAGER("Catalogue-Manager"),
		CATALOGUE_MODERATOR("Catalogue-Moderator"),
		INFRASTRUCTURE_MANAGER("Infrastructure-Manager"),
		DATAMINER_MANAGER("DataMiner-Manager"),
		DATA_MANAGER("Data-Manager"),
		DATA_EDITOR("Data-Editor"),
		VRE_MANAGER("VRE-Manager"),
		VRE_DESIGNER("VRE-Designer"),
		VO_ADMIN("VO-Admin");
			
		private String name;
		
		private GatewayRolesNames(String name) {
			this.name = name;
		}
		
		public String getRoleName() {
			return this.name;
		}
}
