
# Changelog for user management library

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.5.4] - 2022-07-06

 - Feature #23622 added Catalogue-Manager and Moderator roles
 
## [v2.5.3] - 2021-04-12

[#20889] UserManagement enhancement: add the roles "Data-Editor"

## [v2.5.2] - 2020-10-19

Bug Fix 

[#19978] LiferayUserManager#listMembershipRequestsByGroup issue in returned list and improved AcceptMembershipRequests method 

## [v2.5.1]- 2020-07-10

[#19603]Enhance performance of VRE membership requests retrieval by adding dedicated method for retrieving pending requests by groupid

## [v2.5.0] - 2020-03-21

Added getRandomAdmin user method and(improved performance for method for getting listUsersByGroupAndRole

## [v2.4.2] - 2019-02-28

[#16190] Avoid returning Liferay Site Roles in UserManagement Core Library

## [v2.4.0] - 2018-03-08

Create user method automatic set email as verified

Added list users methods by range

Added search users method by groupId and Keywords

## [v2.3.1] - 2017-07-01

Improvements on Liferay's JSON APIs.

## [v2.3.0] - 2017-02-01

Added partial support to Liferay's JSON apis.

## [v2.2.0] - 2016-012-06

Added method to read VirtualGroups associated to sites

[#6115] Fix for bug

## [v2.1.0] - 2016-09-28

Added efficient method to retireve VRE logo URLs

Replaced logging from liferay so that it is possible to	change level at runtime

## [v2.0.0] - 2016-06-23

First release after upgrading to Liferay 6.2

