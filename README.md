# gCube System - User Management Core Library
User Management is a library that sits between the Liferay portal services and gCube portlets. The main functionality of the Library is to provide management of users, roles and organizations. It provides a layer of abstraction above the liferay services, hiding the configuration complexities of roles and organizations. The gcube requirement of roles in the scope of organization is achieved by creating a role with the organization name suffixed to it. The addition/removal of organization names to the role names are handled by the Library and are transparent to the application and portlets using the library.
## Structure of the project

* The source code is present in the src folder. 

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

* https://wiki.gcube-system.org/gcube/UserManagement_Core

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/usermanagement-core/releases).

## Authors

* **Massimiliano Assante** ([ORCID](https://orcid.org/0000-0002-3761-1492)) - [ISTI-CNR Infrascience Group](https://www.isti.cnr.it/People/M.Assante)

## Maintainers

* **Massimiliano Assante** ([ORCID](https://orcid.org/0000-0002-3761-1492)) - [ISTI-CNR Infrascience Group](https://www.isti.cnr.it/People/M.Assante)

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)